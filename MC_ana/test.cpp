#include "SampleAnalyzer/User/Analyzer/test.h"
using namespace MA5;
using namespace std;

// -----------------------------------------------------------------------------
// Initialize
// function called one time at the beginning of the analysis
// -----------------------------------------------------------------------------
bool test::Initialize(const MA5::Configuration& cfg, const std::map<std::string,std::string>& parameters)
{
  INFO << "        <><><><><>   Initialization   <><><><><>" << endmsg;
  // Declare the signal regions
  Manager()->AddRegionSelection("SR");

  // Selection cuts

  // Histogram
  Manager()->AddHisto("Pt[jet(3)]",80,0,1000);
  Manager()->AddHisto("Pt[jet(4)]",80,0,1000);
  Manager()->AddHisto("Pt[jet(5)]",80,0,1000);
  Manager()->AddHisto("n(bjet)",9,-0.5,8.5);
  Manager()->AddHisto("n(jet)",9,-0.5,8.5);
  Manager()->AddHisto("n(jet)_b",9,-0.5,8.5);
  Manager()->AddHisto("MET",80,0,1000);
  Manager()->AddHisto("Imb(b1,b2)",8,0,1);
  Manager()->AddHisto("dPhi(b1,b2)",10,0,2);
  Manager()->AddHisto("dEta(b1,b2)",20,0,10);
  Manager()->AddHisto("M(b1,b2)",100,0,400);
  Manager()->AddHisto("dR(b1,b2)",20,0,4);
  Manager()->AddHisto("Pt(b1,b2)",80,0,1000);
  Manager()->AddHisto("dPhi(bb,MET)",10,0,2);
  Manager()->AddHisto("Pt(bb,MET)",80,0,1000);
  Manager()->AddHisto("dPt(bb,MET)",80,0,1000);
  Manager()->AddHisto("dPhi(trk,MET)",10,0,2);
  Manager()->AddHisto("dPt(trk,MET)",80,0,1000);
  return true;
}

// -----------------------------------------------------------------------------
// Finalize
// function called one time at the end of the analysis
// -----------------------------------------------------------------------------
void test::Finalize(const SampleFormat& summary, const std::vector<SampleFormat>& files)
{
  INFO << "        <><><><><>   Finished   <><><><><>" << endmsg;
}

// -----------------------------------------------------------------------------
// Execute
// function called each time one event is read
// -----------------------------------------------------------------------------
bool test::Execute(SampleFormat& sample, const EventFormat& event)
{
  // Event weight
  double weight=1.;
  if (!Configuration().IsNoEventWeight() && event.mc()!=0) {
    weight=event.mc()->weight();
  }

  Manager()->InitializeForNewEvent(weight);

  if (event.rec()==0) {return true;}

  //Defining the containers
  vector<const RecJetFormat*> BJets, Jets, HJets;
  vector<const RecLeptonFormat*> Muons;
  vector<const RecLeptonFormat*> Electrons;
  vector<const RecTauFormat*> Taus;
  vector<const RecPhotonFormat*> Photons;

  //Jets
  for(unsigned int i=0; i<event.rec()->jets().size(); i++)
  {
    const RecJetFormat *this_jet = &(event.rec()->jets()[i]);
    if (this_jet->pt() > 20. && fabs(this_jet->eta()) < 3.)
    {
      if(this_jet->btag()) BJets.push_back(this_jet);
      Jets.push_back(this_jet);
    }
  }

  SORTER->sort(Jets, PTordering);
  SORTER->sort(BJets, PTordering);

  //Electrons
  for(unsigned int i=0; i<event.rec()->electrons().size(); i++){
    const RecLeptonFormat *this_electron = &(event.rec()->electrons()[i]);
    if(this_electron->pt() > 10. && fabs(this_electron->eta()) < 2.5){
      Electrons.push_back(this_electron);
    }
  }

  //Muons
  for(unsigned int i=0; i<event.rec()->muons().size(); i++){
    const RecLeptonFormat *this_muon = &(event.rec()->muons()[i]);
    if(this_muon->pt() > 10. && fabs(this_muon->eta()) < 2.4){
      Muons.push_back(this_muon);
    }
  }

  //Taus
  for(unsigned int i=0; i<event.rec()->taus().size(); i++){
    const RecTauFormat *this_tau = &(event.rec()->taus()[i]);
    Taus.push_back(this_tau);
  }

  //Photons
  for(unsigned int i=0; i<event.rec()->photons().size(); i++){
    const RecPhotonFormat *this_photon = &(event.rec()->photons()[i]);
    if(this_photon->pt() > 10. && fabs(this_photon->eta()) < 2.5){
      Photons.push_back(this_photon);
    }
  }

  //MET
  MALorentzVector MET = (event.rec()->MET()).momentum();

  //TrkMiss
  MALorentzVector TrkMiss;
  for(unsigned int i=0; i<event.rec()->tracks().size(); i++){
    const RecTrackFormat *this_track = &(event.rec()->tracks()[i]);
    MALorentzVector this_track4vec;
    this_track4vec.SetPxPyPzE(this_track->px(), this_track->py(), this_track->pz(), this_track->e());
    TrkMiss += this_track4vec;
  }
  TrkMiss.SetPxPyPzE(-TrkMiss.Px(), -TrkMiss.Py(), TrkMiss.Pz(), TrkMiss.E());

  double math_pi = 3.141592;
  //Histogram fill
  if(Jets.size()>2) Manager()->FillHisto("Pt[jet(3)]", Jets[2]->pt());
  if(Jets.size()>3) Manager()->FillHisto("Pt[jet(4)]", Jets[3]->pt());
  if(Jets.size()>4) Manager()->FillHisto("Pt[jet(5)]", Jets[4]->pt());
  if(BJets.size()==2) Manager()->FillHisto("n(jet)_b", Jets.size());
  Manager()->FillHisto("n(bjet)", BJets.size());
  Manager()->FillHisto("n(jet)", Jets.size());
  Manager()->FillHisto("MET", MET.Pt());
  Manager()->FillHisto("n(lepton)", Electrons.size() + Muons.size() + Taus.size());
  Manager()->FillHisto("n(photon)", Photons.size());
  if(BJets.size()>1){
    Manager()->FillHisto("Imb(b1,b2)", (BJets[0]->pt() - BJets[1]->pt())/(BJets[0]->pt() + BJets[1]->pt()));
    Manager()->FillHisto("dPhi(b1,b2)", fabs(BJets[0]->dphi_0_pi(BJets[1]))/math_pi);
    Manager()->FillHisto("dEta(b1,b2)", fabs(BJets[0]->eta() - BJets[1]->eta()));
    Manager()->FillHisto("M(b1,b2)", (BJets[0]->momentum()+BJets[1]->momentum()).M());
    Manager()->FillHisto("dR(b1,b2)", BJets[0]->dr(BJets[1]));
    Manager()->FillHisto("Pt(b1,b2)", (BJets[0]->momentum()+BJets[1]->momentum()).Pt());
    Manager()->FillHisto("dPhi(bb,MET)", GetdPhi((BJets[0]->momentum()+BJets[1]->momentum()).Phi(),MET.Phi()));
    Manager()->FillHisto("dPt(bb,MET)", fabs((BJets[0]->momentum()+BJets[1]->momentum()).Pt() - MET.Pt()));
    Manager()->FillHisto("Pt(bb,MET)", (BJets[0]->momentum()+BJets[1]->momentum()+MET).Pt());
  }
  Manager()->FillHisto("dPhi(trk,MET)", GetdPhi(TrkMiss.Phi(),MET.Phi()));
  Manager()->FillHisto("dPt(trk,MET)", fabs(TrkMiss.Pt()-MET.Pt()));
  return true;
}

double test::GetdPhi(double phi1, double phi2){
  double math_pi = 3.141592;
  double dphi = phi1 - phi2;
  if(fabs(dphi) > math_pi) dphi = (fabs(dphi)-2*math_pi);

  return fabs(dphi)/math_pi;
}


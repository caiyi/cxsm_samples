#ifndef analysis_test_h
#define analysis_test_h

#include "SampleAnalyzer/Process/Analyzer/AnalyzerBase.h"

namespace MA5
{
class test : public AnalyzerBase
{
  INIT_ANALYSIS(test,"test")

 public:
  virtual bool Initialize(const MA5::Configuration& cfg, const std::map<std::string,std::string>& parameters);
  virtual void Finalize(const SampleFormat& summary, const std::vector<SampleFormat>& files);
  virtual bool Execute(SampleFormat& sample, const EventFormat& event);

 private:
  double GetdPhi(double phi1, double phi2);
};
}

#endif
#include "SampleAnalyzer/User/Analyzer/final300.h"
using namespace MA5;
using namespace std;

// -----------------------------------------------------------------------------
// Initialize
// function called one time at the beginning of the analysis
// -----------------------------------------------------------------------------
bool final300::Initialize(const MA5::Configuration& cfg, const std::map<std::string,std::string>& parameters)
{
  INFO << "        <><><><><>   Initialization   <><><><><>" << endmsg;
  // Declare the signal regions
  Manager()->AddRegionSelection("300");

  // Selection cuts
  Manager()->AddCut("nlp");
  Manager()->AddCut("nbjet");
  Manager()->AddCut("met");
  Manager()->AddCut("mh");
  Manager()->AddCut("dPt");

  // Histogram
  Manager()->AddHisto("n(bjets)",9,-0.5,8.5);
  Manager()->AddHisto("n(jets)",9,-0.5,8.5);
  Manager()->AddHisto("n(lepton)",9,-0.5,8.5);
  Manager()->AddHisto("n(photon)",9,-0.5,8.5);
  Manager()->AddHisto("MET",80,0,1000);
  Manager()->AddHisto("Imb",8,0,1);
  Manager()->AddHisto("M(bb)",100,0,400);
  Manager()->AddHisto("dR(bb)",20,0,4);
  Manager()->AddHisto("dPt(bb,MET)",80,0,1000);
  return true;
}

// -----------------------------------------------------------------------------
// Finalize
// function called one time at the end of the analysis
// -----------------------------------------------------------------------------
void final300::Finalize(const SampleFormat& summary, const std::vector<SampleFormat>& files)
{
  INFO << "        <><><><><>   Finished   <><><><><>" << endmsg;
}

// -----------------------------------------------------------------------------
// Execute
// function called each time one event is read
// -----------------------------------------------------------------------------
bool final300::Execute(SampleFormat& sample, const EventFormat& event)
{
  // Event weight
  double weight=1.;
  if (!Configuration().IsNoEventWeight() && event.mc()!=0){
    weight=event.mc()->weight();
  }
  
  Manager()->InitializeForNewEvent(weight);

  if (event.rec()==0) {return true;}

  //Defining the containers
  vector<const RecJetFormat*> BJets, Jets;
  vector<const RecLeptonFormat*> Muons;
  vector<const RecLeptonFormat*> Electrons;
  vector<const RecTauFormat*> Taus;
  vector<const RecPhotonFormat*> Photons;

  //Jets
  for(unsigned int i=0; i<event.rec()->jets().size(); i++){
    const RecJetFormat *this_jet = &(event.rec()->jets()[i]);
    //if (this_jet->pt() > 20. && fabs(this_jet->eta()) < 3.)
    if (this_jet->pt() > 20. && this_jet->eta() < 2.5){
      Jets.push_back(this_jet);
      if(this_jet->btag()) {
        BJets.push_back(this_jet);
      }
    }
  }

  SORTER->sort(Jets, PTordering);
  SORTER->sort(BJets, PTordering);

  //Electrons
  for(unsigned int i=0; i<event.rec()->electrons().size(); i++){
    const RecLeptonFormat *this_electron = &(event.rec()->electrons()[i]);
    if(this_electron->pt() > 10. && fabs(this_electron->eta()) < 2.5){
      Electrons.push_back(this_electron);
    }
  }
  
  //Muons
  for(unsigned int i=0; i<event.rec()->muons().size(); i++){
    const RecLeptonFormat *this_muon = &(event.rec()->muons()[i]);
    if(this_muon->pt() > 10. && fabs(this_muon->eta()) < 2.4){
      Muons.push_back(this_muon);
    }
  }

  //Taus
  for(unsigned int i=0; i<event.rec()->taus().size(); i++){
    const RecTauFormat *this_tau = &(event.rec()->taus()[i]);
    Taus.push_back(this_tau);
  }

  //Photons
  for(unsigned int i=0; i<event.rec()->photons().size(); i++){
    const RecPhotonFormat *this_photon = &(event.rec()->photons()[i]);
    if(this_photon->pt() > 10. && fabs(this_photon->eta()) < 2.5){
      Photons.push_back(this_photon);
    }
  }
  
  //MET
  MALorentzVector MET = (event.rec()->MET()).momentum();

  double math_pi = 3.141592;
  //Histogram fill
  Manager()->FillHisto("n(lepton)", Electrons.size() + Muons.size() + Taus.size());
  Manager()->FillHisto("n(photon)", Photons.size()); 
  if (!Manager()->ApplyCut(Photons.size() + Electrons.size() + Muons.size() + Taus.size() == 0, "nlp")) return true;
  Manager()->FillHisto("n(jets)", Jets.size());
  Manager()->FillHisto("n(bjets)", BJets.size());
  if (!Manager()->ApplyCut(BJets.size() == 2 && Jets.size() < 4, "nbjet")) return true;
  Manager()->FillHisto("MET", MET.Pt());
  if (!Manager()->ApplyCut(MET.Pt() > 80, "met")) return true;
  Manager()->FillHisto("M(bb)", (BJets[0]->momentum()+BJets[1]->momentum()).M());
  if (!Manager()->ApplyCut((BJets[0]->momentum()+BJets[1]->momentum()).M()<135. && (BJets[0]->momentum()+BJets[1]->momentum()).M()>110., "mh")) return true;
  Manager()->FillHisto("Imb", (BJets[0]->pt() - BJets[1]->pt())/(BJets[0]->pt() + BJets[1]->pt()));
  Manager()->FillHisto("dPt(bb,MET)", fabs((BJets[0]->momentum()+BJets[1]->momentum()).Pt()-MET.Pt()));
  if (!Manager()->ApplyCut(fabs((BJets[0]->momentum()+BJets[1]->momentum()).Pt()-MET.Pt()) > 50, "dPt")) return true;
  Manager()->FillHisto("dR(bb)", BJets[0]->dr(BJets[1]));

  return true;
}

double final300::GetdPhi(double phi1, double phi2){
  double math_pi = 3.141592;
  double dphi = phi1 - phi2;
  if(fabs(dphi) > math_pi) dphi = (fabs(dphi)-2*math_pi);

  return fabs(dphi)/math_pi;
}


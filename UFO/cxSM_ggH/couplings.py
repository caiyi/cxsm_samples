# This file was automatically created by FeynRules 2.3.43
# Mathematica version: 12.1.1 for Linux x86 (64-bit) (July 15, 2020)
# Date: Sun 29 Aug 2021 16:21:57


from object_library import all_couplings, Coupling

from function_library import complexconjugate, re, im, csc, sec, acsc, asec, cot



GC_1 = Coupling(name = 'GC_1',
                value = '-(Aggh1*COS*complex(0,1))',
                order = {'QED':1})

GC_2 = Coupling(name = 'GC_2',
                value = '(3*d2*complex(0,1))/2.',
                order = {'QED':2})

GC_3 = Coupling(name = 'GC_3',
                value = '(delta2*complex(0,1))/2.',
                order = {'QED':2})

GC_4 = Coupling(name = 'GC_4',
                value = '(COS**2*delta2*complex(0,1))/2.',
                order = {'QED':2})

GC_5 = Coupling(name = 'GC_5',
                value = '-(ee*complex(0,1))/3.',
                order = {'QED':1})

GC_6 = Coupling(name = 'GC_6',
                value = '(2*ee*complex(0,1))/3.',
                order = {'QED':1})

GC_7 = Coupling(name = 'GC_7',
                value = '-(ee*complex(0,1))',
                order = {'QED':1})

GC_8 = Coupling(name = 'GC_8',
                value = 'ee*complex(0,1)',
                order = {'QED':1})

GC_9 = Coupling(name = 'GC_9',
                value = 'ee**2*complex(0,1)',
                order = {'QED':2})

GC_10 = Coupling(name = 'GC_10',
                 value = '-G',
                 order = {'QCD':1})

GC_11 = Coupling(name = 'GC_11',
                 value = 'complex(0,1)*G',
                 order = {'QCD':1})

GC_12 = Coupling(name = 'GC_12',
                 value = '-(Aggh1*COS*G)',
                 order = {'QCD':1,'QED':1})

GC_13 = Coupling(name = 'GC_13',
                 value = 'complex(0,1)*G**2',
                 order = {'QCD':2})

GC_14 = Coupling(name = 'GC_14',
                 value = 'Aggh1*COS*complex(0,1)*G**2',
                 order = {'QCD':2,'QED':1})

GC_15 = Coupling(name = 'GC_15',
                 value = 'Aggh2*complex(0,1)*SIN',
                 order = {'QED':1})

GC_16 = Coupling(name = 'GC_16',
                 value = '(COS*delta2*complex(0,1)*SIN)/2.',
                 order = {'QED':2})

GC_17 = Coupling(name = 'GC_17',
                 value = 'Aggh2*G*SIN',
                 order = {'QCD':1,'QED':1})

GC_18 = Coupling(name = 'GC_18',
                 value = '-(Aggh2*complex(0,1)*G**2*SIN)',
                 order = {'QCD':2,'QED':1})

GC_19 = Coupling(name = 'GC_19',
                 value = '(delta2*complex(0,1)*SIN**2)/2.',
                 order = {'QED':2})

GC_20 = Coupling(name = 'GC_20',
                 value = '(COS*d2*complex(0,1)*SIN)/2. - (COS*delta2*complex(0,1)*SIN)/2.',
                 order = {'QED':2})

GC_21 = Coupling(name = 'GC_21',
                 value = '(COS**2*delta2*complex(0,1))/2. + (d2*complex(0,1)*SIN**2)/2.',
                 order = {'QED':2})

GC_22 = Coupling(name = 'GC_22',
                 value = '(COS**2*d2*complex(0,1))/2. + (delta2*complex(0,1)*SIN**2)/2.',
                 order = {'QED':2})

GC_23 = Coupling(name = 'GC_23',
                 value = '(3*COS**3*delta2*complex(0,1)*SIN)/2. - (3*COS**3*complex(0,1)*lam*SIN)/2. + (3*COS*d2*complex(0,1)*SIN**3)/2. - (3*COS*delta2*complex(0,1)*SIN**3)/2.',
                 order = {'QED':2})

GC_24 = Coupling(name = 'GC_24',
                 value = '(3*COS**3*d2*complex(0,1)*SIN)/2. - (3*COS**3*delta2*complex(0,1)*SIN)/2. + (3*COS*delta2*complex(0,1)*SIN**3)/2. - (3*COS*complex(0,1)*lam*SIN**3)/2.',
                 order = {'QED':2})

GC_25 = Coupling(name = 'GC_25',
                 value = '(3*COS**4*complex(0,1)*lam)/2. + 3*COS**2*delta2*complex(0,1)*SIN**2 + (3*d2*complex(0,1)*SIN**4)/2.',
                 order = {'QED':2})

GC_26 = Coupling(name = 'GC_26',
                 value = '(COS**4*delta2*complex(0,1))/2. + (3*COS**2*d2*complex(0,1)*SIN**2)/2. - 2*COS**2*delta2*complex(0,1)*SIN**2 + (3*COS**2*complex(0,1)*lam*SIN**2)/2. + (delta2*complex(0,1)*SIN**4)/2.',
                 order = {'QED':2})

GC_27 = Coupling(name = 'GC_27',
                 value = '(3*COS**4*d2*complex(0,1))/2. + 3*COS**2*delta2*complex(0,1)*SIN**2 + (3*complex(0,1)*lam*SIN**4)/2.',
                 order = {'QED':2})

GC_28 = Coupling(name = 'GC_28',
                 value = '-((ee**2*complex(0,1))/sw**2)',
                 order = {'QED':2})

GC_29 = Coupling(name = 'GC_29',
                 value = '(COS**2*ee**2*complex(0,1))/(2.*sw**2)',
                 order = {'QED':2})

GC_30 = Coupling(name = 'GC_30',
                 value = '(cw**2*ee**2*complex(0,1))/sw**2',
                 order = {'QED':2})

GC_31 = Coupling(name = 'GC_31',
                 value = '-(COS*ee**2*complex(0,1)*SIN)/(2.*sw**2)',
                 order = {'QED':2})

GC_32 = Coupling(name = 'GC_32',
                 value = '(ee**2*complex(0,1)*SIN**2)/(2.*sw**2)',
                 order = {'QED':2})

GC_33 = Coupling(name = 'GC_33',
                 value = '(ee*complex(0,1))/(sw*cmath.sqrt(2))',
                 order = {'QED':1})

GC_34 = Coupling(name = 'GC_34',
                 value = '(cw*ee*complex(0,1))/(2.*sw)',
                 order = {'QED':1})

GC_35 = Coupling(name = 'GC_35',
                 value = '(cw*ee*complex(0,1))/sw',
                 order = {'QED':1})

GC_36 = Coupling(name = 'GC_36',
                 value = '(-2*cw*ee**2*complex(0,1))/sw',
                 order = {'QED':2})

GC_37 = Coupling(name = 'GC_37',
                 value = '-(ee*complex(0,1)*sw)/(6.*cw)',
                 order = {'QED':1})

GC_38 = Coupling(name = 'GC_38',
                 value = '(ee*complex(0,1)*sw)/(3.*cw)',
                 order = {'QED':1})

GC_39 = Coupling(name = 'GC_39',
                 value = '(-2*ee*complex(0,1)*sw)/(3.*cw)',
                 order = {'QED':1})

GC_40 = Coupling(name = 'GC_40',
                 value = '(ee*complex(0,1)*sw)/cw',
                 order = {'QED':1})

GC_41 = Coupling(name = 'GC_41',
                 value = '-(cw*ee*complex(0,1))/(2.*sw) - (ee*complex(0,1)*sw)/(6.*cw)',
                 order = {'QED':1})

GC_42 = Coupling(name = 'GC_42',
                 value = '(cw*ee*complex(0,1))/(2.*sw) - (ee*complex(0,1)*sw)/(6.*cw)',
                 order = {'QED':1})

GC_43 = Coupling(name = 'GC_43',
                 value = '-(cw*ee*complex(0,1))/(2.*sw) + (ee*complex(0,1)*sw)/(2.*cw)',
                 order = {'QED':1})

GC_44 = Coupling(name = 'GC_44',
                 value = '(cw*ee*complex(0,1))/(2.*sw) + (ee*complex(0,1)*sw)/(2.*cw)',
                 order = {'QED':1})

GC_45 = Coupling(name = 'GC_45',
                 value = 'COS**2*ee**2*complex(0,1) + (COS**2*cw**2*ee**2*complex(0,1))/(2.*sw**2) + (COS**2*ee**2*complex(0,1)*sw**2)/(2.*cw**2)',
                 order = {'QED':2})

GC_46 = Coupling(name = 'GC_46',
                 value = '-(COS*ee**2*complex(0,1)*SIN) - (COS*cw**2*ee**2*complex(0,1)*SIN)/(2.*sw**2) - (COS*ee**2*complex(0,1)*SIN*sw**2)/(2.*cw**2)',
                 order = {'QED':2})

GC_47 = Coupling(name = 'GC_47',
                 value = 'ee**2*complex(0,1)*SIN**2 + (cw**2*ee**2*complex(0,1)*SIN**2)/(2.*sw**2) + (ee**2*complex(0,1)*SIN**2*sw**2)/(2.*cw**2)',
                 order = {'QED':2})

GC_48 = Coupling(name = 'GC_48',
                 value = '(COS*ee**2*complex(0,1)*vev)/(2.*sw**2)',
                 order = {'QED':1})

GC_49 = Coupling(name = 'GC_49',
                 value = '-(ee**2*complex(0,1)*SIN*vev)/(2.*sw**2)',
                 order = {'QED':1})

GC_50 = Coupling(name = 'GC_50',
                 value = 'COS*ee**2*complex(0,1)*vev + (COS*cw**2*ee**2*complex(0,1)*vev)/(2.*sw**2) + (COS*ee**2*complex(0,1)*sw**2*vev)/(2.*cw**2)',
                 order = {'QED':1})

GC_51 = Coupling(name = 'GC_51',
                 value = '-(ee**2*complex(0,1)*SIN*vev) - (cw**2*ee**2*complex(0,1)*SIN*vev)/(2.*sw**2) - (ee**2*complex(0,1)*SIN*sw**2*vev)/(2.*cw**2)',
                 order = {'QED':1})

GC_52 = Coupling(name = 'GC_52',
                 value = '(COS*delta2*complex(0,1)*x0)/2.',
                 order = {'QED':1})

GC_53 = Coupling(name = 'GC_53',
                 value = '(delta2*complex(0,1)*SIN*x0)/2.',
                 order = {'QED':1})

GC_54 = Coupling(name = 'GC_54',
                 value = '-(delta2*complex(0,1)*SIN*vev)/2. + (COS*d2*complex(0,1)*x0)/2.',
                 order = {'QED':1})

GC_55 = Coupling(name = 'GC_55',
                 value = '(COS*delta2*complex(0,1)*vev)/2. + (d2*complex(0,1)*SIN*x0)/2.',
                 order = {'QED':1})

GC_56 = Coupling(name = 'GC_56',
                 value = 'COS**2*delta2*complex(0,1)*SIN*vev - (3*COS**2*complex(0,1)*lam*SIN*vev)/2. - (delta2*complex(0,1)*SIN**3*vev)/2. + (COS**3*delta2*complex(0,1)*x0)/2. + (3*COS*d2*complex(0,1)*SIN**2*x0)/2. - COS*delta2*complex(0,1)*SIN**2*x0',
                 order = {'QED':1})

GC_57 = Coupling(name = 'GC_57',
                 value = '(-3*COS**2*delta2*complex(0,1)*SIN*vev)/2. - (3*complex(0,1)*lam*SIN**3*vev)/2. + (3*COS**3*d2*complex(0,1)*x0)/2. + (3*COS*delta2*complex(0,1)*SIN**2*x0)/2.',
                 order = {'QED':1})

GC_58 = Coupling(name = 'GC_58',
                 value = '(3*COS**3*complex(0,1)*lam*vev)/2. + (3*COS*delta2*complex(0,1)*SIN**2*vev)/2. + (3*COS**2*delta2*complex(0,1)*SIN*x0)/2. + (3*d2*complex(0,1)*SIN**3*x0)/2.',
                 order = {'QED':1})

GC_59 = Coupling(name = 'GC_59',
                 value = '(COS**3*delta2*complex(0,1)*vev)/2. - COS*delta2*complex(0,1)*SIN**2*vev + (3*COS*complex(0,1)*lam*SIN**2*vev)/2. + (3*COS**2*d2*complex(0,1)*SIN*x0)/2. - COS**2*delta2*complex(0,1)*SIN*x0 + (delta2*complex(0,1)*SIN**3*x0)/2.',
                 order = {'QED':1})

GC_60 = Coupling(name = 'GC_60',
                 value = '-((COS*complex(0,1)*yb)/cmath.sqrt(2))',
                 order = {'QED':1})

GC_61 = Coupling(name = 'GC_61',
                 value = '(complex(0,1)*SIN*yb)/cmath.sqrt(2)',
                 order = {'QED':1})

GC_62 = Coupling(name = 'GC_62',
                 value = '-((COS*complex(0,1)*yc)/cmath.sqrt(2))',
                 order = {'QED':1})

GC_63 = Coupling(name = 'GC_63',
                 value = '(complex(0,1)*SIN*yc)/cmath.sqrt(2)',
                 order = {'QED':1})

GC_64 = Coupling(name = 'GC_64',
                 value = '-((COS*complex(0,1)*ydo)/cmath.sqrt(2))',
                 order = {'QED':1})

GC_65 = Coupling(name = 'GC_65',
                 value = '(complex(0,1)*SIN*ydo)/cmath.sqrt(2)',
                 order = {'QED':1})

GC_66 = Coupling(name = 'GC_66',
                 value = '-((COS*complex(0,1)*ye)/cmath.sqrt(2))',
                 order = {'QED':1})

GC_67 = Coupling(name = 'GC_67',
                 value = '(complex(0,1)*SIN*ye)/cmath.sqrt(2)',
                 order = {'QED':1})

GC_68 = Coupling(name = 'GC_68',
                 value = '-((COS*complex(0,1)*ym)/cmath.sqrt(2))',
                 order = {'QED':1})

GC_69 = Coupling(name = 'GC_69',
                 value = '(complex(0,1)*SIN*ym)/cmath.sqrt(2)',
                 order = {'QED':1})

GC_70 = Coupling(name = 'GC_70',
                 value = '-((COS*complex(0,1)*ys)/cmath.sqrt(2))',
                 order = {'QED':1})

GC_71 = Coupling(name = 'GC_71',
                 value = '(complex(0,1)*SIN*ys)/cmath.sqrt(2)',
                 order = {'QED':1})

GC_72 = Coupling(name = 'GC_72',
                 value = '-((COS*complex(0,1)*yt)/cmath.sqrt(2))',
                 order = {'QED':1})

GC_73 = Coupling(name = 'GC_73',
                 value = '(complex(0,1)*SIN*yt)/cmath.sqrt(2)',
                 order = {'QED':1})

GC_74 = Coupling(name = 'GC_74',
                 value = '-((COS*complex(0,1)*ytau)/cmath.sqrt(2))',
                 order = {'QED':1})

GC_75 = Coupling(name = 'GC_75',
                 value = '(complex(0,1)*SIN*ytau)/cmath.sqrt(2)',
                 order = {'QED':1})

GC_76 = Coupling(name = 'GC_76',
                 value = '-((COS*complex(0,1)*yup)/cmath.sqrt(2))',
                 order = {'QED':1})

GC_77 = Coupling(name = 'GC_77',
                 value = '(complex(0,1)*SIN*yup)/cmath.sqrt(2)',
                 order = {'QED':1})

GC_78 = Coupling(name = 'GC_78',
                 value = '(aS*complex(0,1))/(3.*cmath.pi*vev)*(3./2.)',
                 order = {'QCD':2,'QED':1})

GC_79 = Coupling(name = 'GC_79',
                 value = '(aS*complex(0,1))/(3.*cmath.pi*vev)*(3./2.)',
                 order = {'QCD':2,'QED':1})

# This file was automatically created by FeynRules 2.3.43
# Mathematica version: 12.1.1 for Linux x86 (64-bit) (July 15, 2020)
# Date: Sun 29 Aug 2021 16:21:57



from object_library import all_parameters, Parameter


from function_library import complexconjugate, re, im, csc, sec, acsc, asec, cot

# This is a default parameter object representing 0.
ZERO = Parameter(name = 'ZERO',
                 nature = 'internal',
                 type = 'real',
                 value = '0.0',
                 texname = '0')

# User-defined parameters.
aEWM1 = Parameter(name = 'aEWM1',
                  nature = 'external',
                  type = 'real',
                  value = 127.9,
                  texname = '\\text{aEWM1}',
                  lhablock = 'SMINPUTS',
                  lhacode = [ 1 ])

Gf = Parameter(name = 'Gf',
               nature = 'external',
               type = 'real',
               value = 0.0000116637,
               texname = 'G_f',
               lhablock = 'SMINPUTS',
               lhacode = [ 2 ])

aS = Parameter(name = 'aS',
               nature = 'external',
               type = 'real',
               value = 0.1184,
               texname = '\\alpha _s',
               lhablock = 'SMINPUTS',
               lhacode = [ 3 ])

x0 = Parameter(name = 'x0',
               nature = 'external',
               type = 'real',
               value = 40.,
               texname = 'x_0',
               lhablock = 'SMINPUTS',
               lhacode = [ 10 ])

SIN = Parameter(name = 'SIN',
                nature = 'external',
                type = 'real',
                value = 0.02,
                texname = 'S_{\\text{in}}',
                lhablock = 'SMINPUTS',
                lhacode = [ 11 ])

a1 = Parameter(name = 'a1',
               nature = 'external',
               type = 'real',
               value = 1.e6,
               texname = 'a_1',
               lhablock = 'SMINPUTS',
               lhacode = [ 31 ])

ymdo = Parameter(name = 'ymdo',
                 nature = 'external',
                 type = 'real',
                 value = 0.00504,
                 texname = '\\text{ymdo}',
                 lhablock = 'YUKAWA',
                 lhacode = [ 1 ])

ymup = Parameter(name = 'ymup',
                 nature = 'external',
                 type = 'real',
                 value = 0.00255,
                 texname = '\\text{ymup}',
                 lhablock = 'YUKAWA',
                 lhacode = [ 2 ])

yms = Parameter(name = 'yms',
                nature = 'external',
                type = 'real',
                value = 0.101,
                texname = '\\text{yms}',
                lhablock = 'YUKAWA',
                lhacode = [ 3 ])

ymc = Parameter(name = 'ymc',
                nature = 'external',
                type = 'real',
                value = 1.27,
                texname = '\\text{ymc}',
                lhablock = 'YUKAWA',
                lhacode = [ 4 ])

ymb = Parameter(name = 'ymb',
                nature = 'external',
                type = 'real',
                value = 4.7,
                texname = '\\text{ymb}',
                lhablock = 'YUKAWA',
                lhacode = [ 5 ])

ymt = Parameter(name = 'ymt',
                nature = 'external',
                type = 'real',
                value = 172,
                texname = '\\text{ymt}',
                lhablock = 'YUKAWA',
                lhacode = [ 6 ])

yme = Parameter(name = 'yme',
                nature = 'external',
                type = 'real',
                value = 0.000511,
                texname = '\\text{yme}',
                lhablock = 'YUKAWA',
                lhacode = [ 11 ])

ymm = Parameter(name = 'ymm',
                nature = 'external',
                type = 'real',
                value = 0.10566,
                texname = '\\text{ymm}',
                lhablock = 'YUKAWA',
                lhacode = [ 13 ])

ymtau = Parameter(name = 'ymtau',
                  nature = 'external',
                  type = 'real',
                  value = 1.777,
                  texname = '\\text{ymtau}',
                  lhablock = 'YUKAWA',
                  lhacode = [ 15 ])

MZ = Parameter(name = 'MZ',
               nature = 'external',
               type = 'real',
               value = 91.1876,
               texname = '\\text{MZ}',
               lhablock = 'MASS',
               lhacode = [ 23 ])

Me = Parameter(name = 'Me',
               nature = 'external',
               type = 'real',
               value = 0.000511,
               texname = '\\text{Me}',
               lhablock = 'MASS',
               lhacode = [ 11 ])

MMU = Parameter(name = 'MMU',
                nature = 'external',
                type = 'real',
                value = 0.10566,
                texname = '\\text{MMU}',
                lhablock = 'MASS',
                lhacode = [ 13 ])

MTA = Parameter(name = 'MTA',
                nature = 'external',
                type = 'real',
                value = 1.777,
                texname = '\\text{MTA}',
                lhablock = 'MASS',
                lhacode = [ 15 ])

MU = Parameter(name = 'MU',
               nature = 'external',
               type = 'real',
               value = 0.00255,
               texname = 'M',
               lhablock = 'MASS',
               lhacode = [ 2 ])

MC = Parameter(name = 'MC',
               nature = 'external',
               type = 'real',
               value = 1.27,
               texname = '\\text{MC}',
               lhablock = 'MASS',
               lhacode = [ 4 ])

MT = Parameter(name = 'MT',
               nature = 'external',
               type = 'real',
               value = 172,
               texname = '\\text{MT}',
               lhablock = 'MASS',
               lhacode = [ 6 ])

MD = Parameter(name = 'MD',
               nature = 'external',
               type = 'real',
               value = 0.00504,
               texname = '\\text{MD}',
               lhablock = 'MASS',
               lhacode = [ 1 ])

MS = Parameter(name = 'MS',
               nature = 'external',
               type = 'real',
               value = 0.101,
               texname = '\\text{MS}',
               lhablock = 'MASS',
               lhacode = [ 3 ])

MB = Parameter(name = 'MB',
               nature = 'external',
               type = 'real',
               value = 4.7,
               texname = '\\text{MB}',
               lhablock = 'MASS',
               lhacode = [ 5 ])

MH = Parameter(name = 'MH',
               nature = 'external',
               type = 'real',
               value = 125.,
               texname = '\\text{MH}',
               lhablock = 'MASS',
               lhacode = [ 25 ])

Mh2 = Parameter(name = 'Mh2',
                nature = 'external',
                type = 'real',
                value = 80.,
                texname = '\\text{Mh2}',
                lhablock = 'MASS',
                lhacode = [ 1000251 ])

MAh = Parameter(name = 'MAh',
                nature = 'external',
                type = 'real',
                value = 30.,
                texname = '\\text{MAh}',
                lhablock = 'MASS',
                lhacode = [ 1000253 ])

WZ = Parameter(name = 'WZ',
               nature = 'external',
               type = 'real',
               value = 2.4952,
               texname = '\\text{WZ}',
               lhablock = 'DECAY',
               lhacode = [ 23 ])

WW = Parameter(name = 'WW',
               nature = 'external',
               type = 'real',
               value = 2.085,
               texname = '\\text{WW}',
               lhablock = 'DECAY',
               lhacode = [ 24 ])

WT = Parameter(name = 'WT',
               nature = 'external',
               type = 'real',
               value = 1.50833649,
               texname = '\\text{WT}',
               lhablock = 'DECAY',
               lhacode = [ 6 ])

WH = Parameter(name = 'WH',
               nature = 'external',
               type = 'real',
               value = 0.00407,
               texname = '\\text{WH}',
               lhablock = 'DECAY',
               lhacode = [ 25 ])

WH2 = Parameter(name = 'WH2',
                nature = 'external',
                type = 'real',
                value = 2.085,
                texname = '\\text{WH2}',
                lhablock = 'DECAY',
                lhacode = [ 1000251 ])

WA = Parameter(name = 'WA',
               nature = 'external',
               type = 'real',
               value = 2.085,
               texname = '\\text{WA}',
               lhablock = 'DECAY',
               lhacode = [ 1000253 ])

b1 = Parameter(name = 'b1',
               nature = 'internal',
               type = 'real',
               value = '-MAh**2 - (a1*cmath.sqrt(2))/x0',
               texname = 'b_1')

COS = Parameter(name = 'COS',
                nature = 'internal',
                type = 'real',
                value = 'cmath.sqrt(1 - SIN**2)',
                texname = 'C_{\\text{os}}')

aEW = Parameter(name = 'aEW',
                nature = 'internal',
                type = 'real',
                value = '1/aEWM1',
                texname = '\\alpha _{\\text{EW}}')

G = Parameter(name = 'G',
              nature = 'internal',
              type = 'real',
              value = '2*cmath.sqrt(aS)*cmath.sqrt(cmath.pi)',
              texname = 'G')

MW = Parameter(name = 'MW',
               nature = 'internal',
               type = 'real',
               value = 'cmath.sqrt(MZ**2/2. + cmath.sqrt(MZ**4/4. - (aEW*cmath.pi*MZ**2)/(Gf*cmath.sqrt(2))))',
               texname = 'M_W')

d2 = Parameter(name = 'd2',
               nature = 'internal',
               type = 'real',
               value = '(2*(COS**2*Mh2**2 + MH**2*SIN**2 + (a1*cmath.sqrt(2))/x0))/x0**2',
               texname = '\\text{d2}')

ee = Parameter(name = 'ee',
               nature = 'internal',
               type = 'real',
               value = '2*cmath.sqrt(aEW)*cmath.sqrt(cmath.pi)',
               texname = 'e')

sw2 = Parameter(name = 'sw2',
                nature = 'internal',
                type = 'real',
                value = '1 - MW**2/MZ**2',
                texname = '\\text{sw2}')

cw = Parameter(name = 'cw',
               nature = 'internal',
               type = 'real',
               value = 'cmath.sqrt(1 - sw2)',
               texname = 'c_w')

sw = Parameter(name = 'sw',
               nature = 'internal',
               type = 'real',
               value = 'cmath.sqrt(sw2)',
               texname = 's_w')

g1 = Parameter(name = 'g1',
               nature = 'internal',
               type = 'real',
               value = 'ee/cw',
               texname = 'g_1')

gw = Parameter(name = 'gw',
               nature = 'internal',
               type = 'real',
               value = 'ee/sw',
               texname = 'g_w')

vev = Parameter(name = 'vev',
                nature = 'internal',
                type = 'real',
                value = '(2*MW*sw)/ee',
                texname = '\\text{vev}')

Aggh1 = Parameter(name = 'Aggh1',
                  nature = 'internal',
                  type = 'real',
                  value = '-(G**2*(1 + (0.0007738095238095238*MH**6)/ymt**6 + (0.005952380952380952*MH**4)/ymt**4 + (0.058333333333333334*MH**2)/ymt**2))/(12.*cmath.pi**2*vev)',
                  texname = 'A^{\\text{ggh1}}')

Aggh2 = Parameter(name = 'Aggh2',
                  nature = 'internal',
                  type = 'real',
                  value = '-(G**2*(1 + (0.0007738095238095238*Mh2**6)/ymt**6 + (0.005952380952380952*Mh2**4)/ymt**4 + (0.058333333333333334*Mh2**2)/ymt**2))/(12.*cmath.pi**2*vev)',
                  texname = 'A^{\\text{ggh2}}')

delta2 = Parameter(name = 'delta2',
                   nature = 'internal',
                   type = 'real',
                   value = '(2*COS*(MH**2 - Mh2**2)*SIN)/(vev*x0)',
                   texname = '\\text{delta2}')

lam = Parameter(name = 'lam',
                nature = 'internal',
                type = 'real',
                value = '(2*(COS**2*MH**2 + Mh2**2*SIN**2))/vev**2',
                texname = '\\text{lam}')

yb = Parameter(name = 'yb',
               nature = 'internal',
               type = 'real',
               value = '(ymb*cmath.sqrt(2))/vev',
               texname = '\\text{yb}')

yc = Parameter(name = 'yc',
               nature = 'internal',
               type = 'real',
               value = '(ymc*cmath.sqrt(2))/vev',
               texname = '\\text{yc}')

ydo = Parameter(name = 'ydo',
                nature = 'internal',
                type = 'real',
                value = '(ymdo*cmath.sqrt(2))/vev',
                texname = '\\text{ydo}')

ye = Parameter(name = 'ye',
               nature = 'internal',
               type = 'real',
               value = '(yme*cmath.sqrt(2))/vev',
               texname = '\\text{ye}')

ym = Parameter(name = 'ym',
               nature = 'internal',
               type = 'real',
               value = '(ymm*cmath.sqrt(2))/vev',
               texname = '\\text{ym}')

ys = Parameter(name = 'ys',
               nature = 'internal',
               type = 'real',
               value = '(yms*cmath.sqrt(2))/vev',
               texname = '\\text{ys}')

yt = Parameter(name = 'yt',
               nature = 'internal',
               type = 'real',
               value = '(ymt*cmath.sqrt(2))/vev',
               texname = '\\text{yt}')

ytau = Parameter(name = 'ytau',
                 nature = 'internal',
                 type = 'real',
                 value = '(ymtau*cmath.sqrt(2))/vev',
                 texname = '\\text{ytau}')

yup = Parameter(name = 'yup',
                nature = 'internal',
                type = 'real',
                value = '(ymup*cmath.sqrt(2))/vev',
                texname = '\\text{yup}')

b1Ab2 = Parameter(name = 'b1Ab2',
                  nature = 'internal',
                  type = 'real',
                  value = '-(delta2*vev**2)/2. - (d2*x0**2)/2. - (2*a1*cmath.sqrt(2))/x0',
                  texname = '\\text{b1A}_{b_2}')

mu2 = Parameter(name = 'mu2',
                nature = 'internal',
                type = 'real',
                value = '(-(lam*vev**2) - delta2*x0**2)/2.',
                texname = '\\text{$\\mu $2}')

b2 = Parameter(name = 'b2',
               nature = 'internal',
               type = 'real',
               value = '-b1 + b1Ab2',
               texname = 'b_2')

ybrs2e = Parameter(name = 'ybrs2e',
                nature = 'internal',
                type = 'real',
                value = 'SIN', 
                texname = '\\text{ybrs2e}')

ytrs2e = Parameter(name = 'ytrs2e',
                nature = 'internal',
                type = 'real',
                value = 'SIN',                               
                texname = '\\text{ytrs2e}')

ybrs1e = Parameter(name = 'ybrs1e',
                nature = 'internal',
                type = 'real',
                value = 'COS', 
                texname = '\\text{ybrs1e}')
    
ytrs1e = Parameter(name = 'ytrs1e',
                nature = 'internal',
                type = 'real',
                value = 'COS',                               
                texname = '\\text{ytrs1e}')




#include <cmath>
#include <fstream>
#include <iostream>
#include <string>
#include <cstdlib>
#include <iomanip>
#include <fstream>
#include <map>
#include <complex>
#include <stdexcept>
#include <vector>
#include <stdio.h>
#include <stdlib.h>
#include <sstream>
#include <iomanip>
#include <iostream>
#include <fstream>
#include <math.h> 
#include <exception>
#include <cstdlib>
#include <cstdio>
#include <algorithm>
#include <stdlib.h> 
#include <sstream> 
#include <time.h>
#include <unistd.h>

#include "TROOT.h"
#include "TClonesArray.h" 
#include "TRint.h"
#include "TH1F.h"
#include "TH2F.h"
#include "TF1.h"
#include "TCanvas.h"
#include "TChain.h"
#include "TVector3.h"
#include "TMath.h"

#include "TVector3.h"
#include "TMath.h"
#include "TFile.h"
#include "TSystem.h"
#include "TGStatusBar.h"
#include "TSystem.h"
#include "TXMLEngine.h"
#include "TTree.h"
#include "TLorentzVector.h"
#include "TNtuple.h"
#include "TStyle.h"
#include "TGraphErrors.h"
#include "TGraph.h"

#include "classes/ClassesLinkDef.h"
#include "classes/DelphesClasses.h"
#include "classes/DelphesFactory.h"
#include "classes/DelphesModule.h"
#include "classes/DelphesStream.h"

#include "ExRootAnalysis/ExRootTreeReader.h"
#include "ExRootAnalysis/ExRootAnalysisLinkDef.h"
#include "ExRootAnalysis/ExRootProgressBar.h"
#include "ExRootAnalysis/ExRootTask.h"
#include "ExRootAnalysis/ExRootTreeBranch.h"

#include "fastjet/PseudoJet.hh"
#include "fastjet/ClusterSequence.hh"
#include "fastjet/internal/BasicRandom.hh"
#include "fastjet/JetDefinition.hh"
#include "fastjet/tools/MassDropTagger.hh"
#include "fastjet/tools/Filter.hh"   // needed for HEPTopTagger
#include "fastjet/tools/Pruner.hh"   // needed for HEPTopTagger
#include "fastjet/tools/JHTopTagger.hh"

using namespace std;
using namespace TMath;
using namespace fastjet;

bool sortPt(const TLorentzVector a, const TLorentzVector b) {return (a.Pt() < b.Pt());}

double GetdPhi(double phi1, double phi2){
  double math_pi = 3.14159265358979;
  double dphi = phi1 - phi2;
  if(fabs(dphi) > math_pi) dphi = (fabs(dphi)-2*math_pi);

  return fabs(dphi)/math_pi;
}

double Imb(double pt1, double pt2) {return (fabs(pt1-pt2)/(pt1+pt2));}

vector<string> split(const string &str, const char pattern){
    vector<string> res;
    stringstream input(str);
    string temp;
    while(getline(input, temp, pattern))
    {
        res.push_back(temp);
    }
    return res;
}

//Mode should be selected from ('_300','_500')
vector<double> BBMET(TString inputFileName, double xsec, int cases, double value){
    // Create chain of root trees
    TChain chain("Delphes");
    string name = split(split(string(inputFileName),'/').back(),'.')[0];
    if (string(inputFileName).find("root") == string::npos){
        chain.Add(inputFileName+"/Events/run_01/tag_1_delphes_events.root");
        chain.Add(inputFileName+"/Events/run_02/tag_1_delphes_events.root");
        chain.Add(inputFileName+"/Events/run_03/tag_1_delphes_events.root");
    }
    else {chain.Add(inputFileName);}

    // Create object of class ExRootTreeReader
    ExRootTreeReader *treeReader = new ExRootTreeReader(&chain);
    Long64_t numberOfEntries = treeReader->GetEntries();
    TClonesArray *branchEvent = treeReader->UseBranch("Event");
    TClonesArray *branchJet = treeReader->UseBranch("Jet");
    TClonesArray *branchMissingET = treeReader->UseBranch("MissingET");
    TClonesArray *branchPhoton = treeReader->UseBranch("Photon");
    TClonesArray *branchElectron = treeReader->UseBranch("Electron");
    TClonesArray *branchMuon = treeReader->UseBranch("Muon");
    Jet* jet;
    Photon* photon;
    Electron* electron;
    Muon* muon;
    MissingET* met;
    Track* track;
    
    string path1=("./data/left_"), path2=(".dat");
    string path=(path1.append(name)).append(path2);
    string path3="./total.dat";
    ofstream total;
    total.open(path3, ios::app);
    double cut_=0.0, cut_nlp=0.0, cut_njet=0.0, cut_mh=0.0, cut_met=0.0, cut_pth=0.0, cut_imb=0.0;
    double total_weight = 0.0;
    double weight_nlp=0.0, weight_njet=0.0, weight_mh=0.0, weight_met=0.0, weight_pth=0.0, weight_imb=0.0;

    for(int entry = 0; entry < numberOfEntries; ++entry){
        treeReader->ReadEntry(entry);
        int fl_nlp=0, fl_njet=0, fl_mh=0, fl_met=0, fl_pth=0, fl_imb=0;

        //Event weight
        HepMCEvent *event = (HepMCEvent*) branchEvent -> At(0);
        float weight =  event->Weight;
        total_weight += weight;

        //Containers filling
        vector<TLorentzVector> All_jet, All_bjet, All_photon, All_lepton;
        TLorentzVector MET, TrkMiss;

        for(int i=0; i<branchJet->GetEntriesFast(); i++){
            jet = (Jet*) branchJet -> At(i);
            TLorentzVector tjet;
            tjet = jet->P4();
            if (jet->TauTag) All_lepton.push_back(tjet);
            if (jet->PT > 20. && fabs(jet->Eta) < 2.5){
                All_jet.push_back(tjet); 
                if(jet->BTag) All_bjet.push_back(tjet); 
            }
        }
        sort(All_bjet.begin(), All_bjet.end(), sortPt);

        for(int i=0; i<branchPhoton->GetEntriesFast(); i++){
            photon = (Photon*) branchPhoton -> At(i);
            if (photon->PT > 10. && fabs(photon->Eta) < 2.5){
                TLorentzVector tphoton;
                tphoton = photon->P4();
                All_photon.push_back(tphoton);
            }
        }

        for(int i=0; i<branchElectron->GetEntriesFast(); i++){
            electron = (Electron*) branchElectron -> At(i);
            if (electron->PT > 10. && fabs(electron->Eta) < 2.5){
                TLorentzVector telectron;
                telectron = electron->P4();
                All_lepton.push_back(telectron);
            }
        }

        for(int i=0; i<branchMuon->GetEntriesFast(); i++){
            muon = (Muon*) branchMuon -> At(i);
            if (muon->PT > 10. && fabs(muon->Eta) < 2.4){
                TLorentzVector tmuon;
                tmuon = muon->P4();
                All_lepton.push_back(tmuon);
            }
        }

        if(branchMissingET->GetEntriesFast() > 0){
            met = (MissingET*) branchMissingET->At(0);
            MET = met->P4();
        }

        double math_pi = 3.14159265358979;
        double DeltaPhiJet=99999.;
        for(unsigned int kr=0; kr<All_jet.size();kr++)
            if(abs(All_jet[kr].DeltaPhi(MET))/math_pi<DeltaPhiJet){
                DeltaPhiJet=abs(All_jet[kr].DeltaPhi(MET))/math_pi;
            }
        float deta = 0;
        float dPhi = 0;
        if (All_bjet.size() == 2){
            deta = fabs(All_bjet[0].Eta()-All_bjet[1].Eta());
            dPhi = fabs(All_bjet[0].DeltaPhi(All_bjet[1]))/math_pi;
        }
        double dRmin = 99999.;
        double dRmax = 0.;
        if (All_jet.size() > 2) {
            for (unsigned int ir=0;ir<All_jet.size();ir++)
                for (unsigned int jr=ir+1;jr<All_jet.size();jr++){
                    if(All_jet[ir].DeltaR(All_jet[jr])<dRmin){
                        dRmin=All_jet[ir].DeltaR(All_jet[jr]);
                    }
                    if(All_jet[ir].DeltaR(All_jet[jr])>dRmax){
                        dRmax=All_jet[ir].DeltaR(All_jet[jr]);
                    }
                }
        }
        
        if (All_lepton.size() == 0 && All_photon.size() == 0) fl_nlp = 1;
        if (All_bjet.size() == 2 && All_jet.size() < 4) fl_njet = 1;
        if (All_bjet.size() == 2 && ((All_bjet[0] + All_bjet[1]).M() < 140.) && ((All_bjet[0] + All_bjet[1]).M() > 105.)) fl_mh = 1;
        switch(cases){
            case 1:{if (MET.Pt() > 15.) fl_met = 1;if (dRmin>0.6) fl_imb = 1;fl_pth = 1;break;}
            case 2:{if (MET.Pt() > 165.) fl_met = 1;if (dRmin>0.55) fl_imb = 1;fl_pth = 1;break;}
            case 3:{if (MET.Pt() > 210.) fl_met = 1;fl_imb = 1;fl_pth = 1;break;}
            case 4:{if (MET.Pt() > 210.) fl_met = 1;fl_imb = 1;fl_pth = 1;break;}
            case 5:{if (MET.Pt() > 280.) fl_met = 1;if (dRmin<3.3) fl_imb = 1;fl_pth = 1;break;}
            case 6:{if (MET.Pt() > 365.) fl_met = 1;if (dRmin<3.3) fl_imb = 1;fl_pth = 1;break;}
        }
        //if (DeltaPhiJet>0.3) fl_pth = 1
        //fl_met = 1;fl_imb = 1;fl_pth = 1;

        cut_+=1.0;
        if(fl_nlp==1){cut_nlp+=1.0;weight_nlp+=weight;}
        if(fl_nlp==1 && fl_njet==1){cut_njet+=1.0;weight_njet+=weight;}
        if(fl_nlp==1 && fl_njet==1 && fl_mh==1){cut_mh+=1.0;weight_mh+=weight;}
        if(fl_nlp==1 && fl_njet==1 && fl_mh==1 && fl_met==1){cut_met+=1.0;weight_met+=weight;}
        if(fl_nlp==1 && fl_njet==1 && fl_mh==1 && fl_met==1 && fl_imb==1){cut_imb+=1.0;weight_imb+=weight;}
        //if(fl_nlp==1 && fl_njet==1 && fl_mh==1 && fl_met==1 && fl_imb==1 && fl_pth==1){cut_pth+=1.0;weight_pth+=weight;}
    }

    vector<double> Ratio;
    //double R_nlp=cut_nlp/cut_, R_njet=cut_njet/cut_, R_mh=cut_mh/cut_, R_met=cut_met/cut_, R_imb=cut_imb/cut_, R_pth=cut_pth/cut_;
    double R_nlp=weight_nlp/total_weight, R_njet=weight_njet/total_weight, R_mh=weight_mh/total_weight, R_met=weight_met/total_weight, R_imb=weight_imb/total_weight, R_pth=weight_pth/total_weight;
    Ratio.push_back(R_nlp); Ratio.push_back(R_njet); Ratio.push_back(R_mh); Ratio.push_back(R_met); Ratio.push_back(R_imb); Ratio.push_back(R_pth);

    total<<cases<<"  "<<name<<"  EventNumbers="<<numberOfEntries<<"  TotalWeight="<<total_weight<<endl;
    total<<"Ratio_N(l+p)            = "<<Ratio[0]<<"  "<<Ratio[0]/1.<<"  "<<Ratio[0]*xsec*3000000<<endl;
    total<<"Ratio_N(jet)            = "<<Ratio[1]<<"  "<<Ratio[1]/Ratio[0]<<"  "<<Ratio[1]*xsec*3000000<<endl;
    total<<"Ratio_M(bb)             = "<<Ratio[2]<<"  "<<Ratio[2]/Ratio[1]<<"  "<<Ratio[2]*xsec*3000000<<endl;
    total<<"Ratio_MET               = "<<Ratio[3]<<"  "<<Ratio[3]/Ratio[2]<<"  "<<Ratio[3]*xsec*3000000<<endl;
    total<<"Ratio_bJetSplit         = "<<Ratio[4]<<"  "<<Ratio[4]/Ratio[3]<<"  "<<Ratio[4]*xsec*3000000<<endl;
    total<<"Ratio_dRmin             = "<<Ratio[5]<<"  "<<Ratio[5]/Ratio[4]<<"  "<<Ratio[5]*xsec*3000000<<endl;
    total<<endl;

    return Ratio;
}

void round(int cases, double value){
    vector<double> Ratio_Cutflow_s,Ratio_Cutflow_1,Ratio_Cutflow_2,Ratio_Cutflow_3,Ratio_Cutflow_4,Ratio_Cutflow_5,Ratio_Cutflow_6,Ratio_Cutflow_7,Ratio_Cutflow_8;
    TString inputFileName;
    switch(cases){
        case 1:{inputFileName=TString("/fsa/home/sjc_caiyz/samples/signal/s_300_2");break;}
        case 2:{inputFileName=TString("/fsa/home/sjc_caiyz/samples/signal/s_400_2");break;}
        case 3:{inputFileName=TString("/fsa/home/sjc_caiyz/samples/signal/s_500_2");break;}
        case 4:{inputFileName=TString("/fsa/home/sjc_caiyz/samples/signal/s_500_3");break;}
        case 5:{inputFileName=TString("/fsa/home/sjc_caiyz/samples/signal/s_700_3");break;}
        case 6:{inputFileName=TString("/fsa/home/sjc_caiyz/samples/signal/s_1000_3");break;}
    }   
    cout<<inputFileName<<endl;
    Ratio_Cutflow_s=BBMET(inputFileName,1,cases,value);
    Ratio_Cutflow_1=BBMET("/fsa/home/sjc_caiyz/samples/ttbar/pp_ttbar.root",98.01431,cases,value);
    Ratio_Cutflow_2=BBMET("/fsa/home/sjc_caiyz/samples/VH/VH_W_lv.root",0.3435,cases,value);
    Ratio_Cutflow_3=BBMET("/fsa/home/sjc_caiyz/samples/VH/VH_Z_vv.root",0.1168,cases,value);
    Ratio_Cutflow_4=BBMET("/fsa/home/sjc_caiyz/samples/Vjj/Wjj.root",45871.3,cases,value);
    Ratio_Cutflow_5=BBMET("/fsa/home/sjc_caiyz/samples/Vjj/Zjj.root",12810.5,cases,value);
    Ratio_Cutflow_6=BBMET("/fsa/home/sjc_caiyz/samples/WZ/pp_WZ_bblv.root",5.8,cases,value);
    Ratio_Cutflow_7=BBMET("/fsa/home/sjc_caiyz/samples/WZ/pp_WZ_bjvv.root",15.89,cases,value);
    Ratio_Cutflow_8=BBMET("/fsa/home/sjc_caiyz/samples/WZ/pp_ZZ_bbvv.root",2.58,cases,value);

    double total_bkg = 3000000*(Ratio_Cutflow_1.back()*98.01431+Ratio_Cutflow_2.back()*0.3435+Ratio_Cutflow_3.back()*0.1168+Ratio_Cutflow_4.back()*45871.3+Ratio_Cutflow_5.back()*12810.5+Ratio_Cutflow_6.back()*5.8+Ratio_Cutflow_7.back()*15.89+Ratio_Cutflow_8.back()*2.58);
    double cls = 1.96*pow(total_bkg,0.5)/(Ratio_Cutflow_s.back()*3000000);
    cout<<value<<"  6  "<<total_bkg<<"  "<<cls<<endl;
    Ratio_Cutflow_s.pop_back();Ratio_Cutflow_1.pop_back();Ratio_Cutflow_2.pop_back();Ratio_Cutflow_3.pop_back();Ratio_Cutflow_4.pop_back();Ratio_Cutflow_5.pop_back();Ratio_Cutflow_6.pop_back();Ratio_Cutflow_7.pop_back();Ratio_Cutflow_8.pop_back();
    total_bkg = 3000000*(Ratio_Cutflow_1.back()*98.01431+Ratio_Cutflow_2.back()*0.3435+Ratio_Cutflow_3.back()*0.1168+Ratio_Cutflow_4.back()*45871.3+Ratio_Cutflow_5.back()*12810.5+Ratio_Cutflow_6.back()*5.8+Ratio_Cutflow_7.back()*15.89+Ratio_Cutflow_8.back()*2.58);
    cls = 1.96*pow(total_bkg,0.5)/(Ratio_Cutflow_s.back()*3000000);
    cout<<"5  "<<total_bkg<<"  "<<cls<<endl;
    Ratio_Cutflow_s.pop_back();Ratio_Cutflow_1.pop_back();Ratio_Cutflow_2.pop_back();Ratio_Cutflow_3.pop_back();Ratio_Cutflow_4.pop_back();Ratio_Cutflow_5.pop_back();Ratio_Cutflow_6.pop_back();Ratio_Cutflow_7.pop_back();Ratio_Cutflow_8.pop_back();
    total_bkg = 3000000*(Ratio_Cutflow_1.back()*98.01431+Ratio_Cutflow_2.back()*0.3435+Ratio_Cutflow_3.back()*0.1168+Ratio_Cutflow_4.back()*45871.3+Ratio_Cutflow_5.back()*12810.5+Ratio_Cutflow_6.back()*5.8+Ratio_Cutflow_7.back()*15.89+Ratio_Cutflow_8.back()*2.58);
    cls = 1.96*pow(total_bkg,0.5)/(Ratio_Cutflow_s.back()*3000000);
    cout<<"4  "<<total_bkg<<"  "<<cls<<endl;
    Ratio_Cutflow_s.pop_back();Ratio_Cutflow_1.pop_back();Ratio_Cutflow_2.pop_back();Ratio_Cutflow_3.pop_back();Ratio_Cutflow_4.pop_back();Ratio_Cutflow_5.pop_back();Ratio_Cutflow_6.pop_back();Ratio_Cutflow_7.pop_back();Ratio_Cutflow_8.pop_back();
    total_bkg = 3000000*(Ratio_Cutflow_1.back()*98.01431+Ratio_Cutflow_2.back()*0.3435+Ratio_Cutflow_3.back()*0.1168+Ratio_Cutflow_4.back()*45871.3+Ratio_Cutflow_5.back()*12810.5+Ratio_Cutflow_6.back()*5.8+Ratio_Cutflow_7.back()*15.89+Ratio_Cutflow_8.back()*2.58);
    cls = 1.96*pow(total_bkg,0.5)/(Ratio_Cutflow_s.back()*3000000);
    cout<<"3  "<<total_bkg<<"  "<<cls<<endl;
    Ratio_Cutflow_s.pop_back();Ratio_Cutflow_1.pop_back();Ratio_Cutflow_2.pop_back();Ratio_Cutflow_3.pop_back();Ratio_Cutflow_4.pop_back();Ratio_Cutflow_5.pop_back();Ratio_Cutflow_6.pop_back();Ratio_Cutflow_7.pop_back();Ratio_Cutflow_8.pop_back();
    total_bkg = 3000000*(Ratio_Cutflow_1.back()*98.01431+Ratio_Cutflow_2.back()*0.3435+Ratio_Cutflow_3.back()*0.1168+Ratio_Cutflow_4.back()*45871.3+Ratio_Cutflow_5.back()*12810.5+Ratio_Cutflow_6.back()*5.8+Ratio_Cutflow_7.back()*15.89+Ratio_Cutflow_8.back()*2.58);
    cls = 1.96*pow(total_bkg,0.5)/(Ratio_Cutflow_s.back()*3000000);
    cout<<"2  "<<total_bkg<<"  "<<cls<<endl;
    Ratio_Cutflow_s.pop_back();Ratio_Cutflow_1.pop_back();Ratio_Cutflow_2.pop_back();Ratio_Cutflow_3.pop_back();Ratio_Cutflow_4.pop_back();Ratio_Cutflow_5.pop_back();Ratio_Cutflow_6.pop_back();Ratio_Cutflow_7.pop_back();Ratio_Cutflow_8.pop_back();
    total_bkg = 3000000*(Ratio_Cutflow_1.back()*98.01431+Ratio_Cutflow_2.back()*0.3435+Ratio_Cutflow_3.back()*0.1168+Ratio_Cutflow_4.back()*45871.3+Ratio_Cutflow_5.back()*12810.5+Ratio_Cutflow_6.back()*5.8+Ratio_Cutflow_7.back()*15.89+Ratio_Cutflow_8.back()*2.58);
    cls = 1.96*pow(total_bkg,0.5)/(Ratio_Cutflow_s.back()*3000000);
    cout<<"1  "<<total_bkg<<"  "<<cls<<endl;

    /*for(int i=0; i<Ratio_Cutflow_s.size(); i++){
        double total_bkg = 3000000*(Ratio_Cutflow_1[i]*89+Ratio_Cutflow_2[i]*0.3+Ratio_Cutflow_3[i]*0.2+Ratio_Cutflow_4[i]*45871+Ratio_Cutflow_5[i]*12810+Ratio_Cutflow_6[i]*1.58+Ratio_Cutflow_7[i]*3.02+Ratio_Cutflow_8[i]*4.2);
        double cls = 1.96*pow(total_bkg,0.5)/(Ratio_Cutflow_s[i]*3000000);
        cout<<i+1<<":  "<<Ratio_Cutflow_s[i]<<"  "<<total_bkg<<"  "<<cls<<endl;
    }*/
}

int main(int argc, char *argv[])
{   
    //TString inputFileName(argv[1]);
    int cases = atoi(argv[1]);
    double value;
    vector<double> Ratio_Cutflow_s,Ratio_Cutflow_1,Ratio_Cutflow_2,Ratio_Cutflow_3,Ratio_Cutflow_4,Ratio_Cutflow_5,Ratio_Cutflow_6,Ratio_Cutflow_7,Ratio_Cutflow_8;
    cout<<"1#"<<endl;
    value=0.1; round(cases,value);
    /*cout<<"2#"<<endl;
    value=0.2; round(cases,value);
    cout<<"3#"<<endl;
    value=0.3; round(cases,value);
    cout<<"4#"<<endl;
    value=0.4; round(cases,value);
    cout<<"5#"<<endl;
    value=0.5; round(cases,value);
    cout<<"6#"<<endl;
    value=0.6; round(cases,value);
    cout<<"7#"<<endl;
    value=0.7; round(cases,value);
    cout<<"8#"<<endl;
    value=0.8; round(cases,value);
    cout<<"9#"<<endl;
    value=0.9; round(cases,value);
    cout<<"10#"<<endl;
    value=1.0; round(cases,value);
    cout<<"11#"<<endl;
    value=1.1; round(cases,value);
    cout<<"12#"<<endl;
    value=1.2; round(cases,value);
    cout<<"13#"<<endl;
    value=1.3; round(cases,value);
    cout<<"14#"<<endl;
    value=1.4; round(cases,value);
    cout<<"15#"<<endl;
    value=1.5; round(cases,value);
    cout<<"16#"<<endl;
    value=1.6; round(cases,value);
    return 0;*/
}

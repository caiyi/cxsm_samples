#include "Pythia8/Pythia.h"
#include "Pythia8Plugins/HepMC2.h"
#include "Pythia8/HadronLevel.h"
#include <cstdio>
#include <fstream>
#include <sstream>
#include <cmath>
#include <string>
#include <vector>
#include <cstdlib>
using namespace Pythia8;
using namespace std;
using std::vector;
int main() {

  
  // Interface for conversion from Pythia8::Event to HepMC event.
  HepMC::Pythia8ToHepMC ToHepMC;

  // Specify file where HepMC events will be stored.
  HepMC::IO_GenEvent ascii_io("/fsa/home/sjc_caiyz/samples/WZ/pp_ZZ_bbvv.hepmc", std::ios::out);

  // Intialize Pythia.
  Pythia pythia;

  // Process selection.
  //pythia.readString("WeakSingleBoson:ffbar2gmZ = on");
  pythia.readString("WeakDoubleBoson:ffbar2gmZgmZ = on");
  pythia.readString("WeakZ0:gmZmode=2");
  pythia.readFile("pp_WZ.cmnd");
  pythia.readString("PhaseSpace:pTHatMin = 5.");
  // CEPC initialization at 240 GeV.
  pythia.readString("Beams:idA =  2212");
  pythia.readString("Beams:idB =  2212");
  //double mZ = pythia.particleData.m0(23);
  pythia.settings.parm("Beams:eCM", 14000);
  pythia.readString("23:onMode = off");
  pythia.readString("23:onIfAny = 5 12 14 16");

  pythia.readString("Random:setSeed = on");
  pythia.readString("Random:seed = 999");

  pythia.init();

  // Set the number of events in the generator file.
  int nEvtPerFile = 5000000;

  // The event loop.
  //Hist mass("m(e, pi0) [GeV]", 100, 0., 2.);
  //Hist mult("charged multiplicity", 100, -0.5, 799.5);
  for (int iEvent = 0; iEvent < nEvtPerFile; ) {
    // Generate the event.
    if (!pythia.next()) continue;
    // Analyze the event.
    //Event &event = pythia.event;
	// Construct new empty HepMC event and fill it.
	// Units will be as chosen for HepMC build; but can be changed
	// by arguments, e.g. GenEvt( HepMC::Units::GEV, HepMC::Units::MM)
	
	// Write the HepMC event to file. Done with it.
	HepMC::GenEvent* hepmcevt = new HepMC::GenEvent();
	ToHepMC.fill_next_event( pythia, hepmcevt );
	ascii_io << hepmcevt;


	iEvent++;
	delete hepmcevt;
  }

  // Print the statistics and histogram.
  pythia.stat();
  return 0;
}

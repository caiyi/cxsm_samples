#!/usr/bin/env python3
from datetime import date
from subprocess import call
import argparse
import os
import shutil
cwd = os.getcwd()

n = '1000000'
s = '-0.2'
v = '40'
a = '-1000000'
mh = '1000'
type = 's3'
if (type == 's2'):
    ma = '62.49'
elif (type == 's3'):
    ma = '150'
else:
    s = '0'
    v = '0'
    a = '0'
    mh = '0'
    ma = '0'

def makecommand():
    runstr = cwd + "/run.py -n {:s}  --sin_theta {:s} --vev_s {:s} --a_1 {:s} --m_h2 {:s} --m_ah {:s} -t {:s} ".format(n,s,v,a,mh,ma,type)
    return runstr
commands = makecommand()
name = "MG_{:s}_{:s}_{:s}_{:s}_{:s}_{:s}".format(s,v,a,mh,ma,type)
tmp = """
#BSUB -q e74820v4
#BSUB -n 12
#BSUB -J {:s}
#BSUB -o out
#BSUB -e err
source /fsa/home/sjc_caiyz/samples/signal/setup.sh
OMP_NUM_THREADS="$LSB_DJOB_NUMPROC"
{:s}
""".format(name,commands)

if not os.path.exists(os.path.join(cwd, "job", name)):
    os.makedirs(os.path.join(cwd, "job", name))
else:
    shutil.rmtree(os.path.join(cwd, "job", name))
    os.makedirs(os.path.join(cwd, "job", name))

exc = open('job/'+name+'/job.lsf', 'w')
exc.write(tmp)
exc.close()
call(['chmod', '+x', 'job/'+name+'/job.lsf'])

excscript = open('condor.sh', 'w')
excscript.write('cd job/'+name+'\n')
excscript.write('bsub < job.lsf \n')
excscript.write('cd ../..')
excscript.close()
call(['chmod', '+x', 'condor.sh'])

call(['./condor.sh'],shell=True)


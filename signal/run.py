#!/usr/bin/env python3
import sys, subprocess, argparse, os, shutil

if __name__ == "__main__":

  parser = argparse.ArgumentParser(formatter_class=argparse.ArgumentDefaultsHelpFormatter)
  parser.add_argument("-i", "--iseed", type = int, default = 1)
  parser.add_argument("-n", "--n_events", type = int, default = 10)
  parser.add_argument("-e", "--ecm", type = int, default = 14000)
  parser.add_argument("--sin_theta", type = float, default = 0.02)
  parser.add_argument("--vev_s", default = 40)
  parser.add_argument("--a_1", default = 1000000)
  parser.add_argument("--m_h2", default = 80)
  parser.add_argument("--m_ah", default = 60)
  parser.add_argument("-t", "--type", default = "s2")
  arg = parser.parse_args()

  cudir = os.getcwd()
  dir = os.path.join("/fsa/home/sjc_caiyz/","MG5_aMC")
  filename = "MG5_{r.sin_theta}_{r.vev_s}_{r.a_1}_{r.m_h2}_{r.m_ah}_{r.type}".format(r = arg)
  output_path = os.path.join("/fsa/home/sjc_caiyz/samples/signal",filename)
  if arg.type == "s2":
    model = "cxSM"
    process = "p p > h2, (h2 > H1 H1, (H1 > b b~), (H1 > ~Ah ~Ah))"
  elif arg.type == "s3":
    model = "cxSM"
    process = "p p > h2, (h2 > H1 H1 > H1 ~Ah ~Ah $ H1, (H1 > b b~))"
  if not os.path.exists(output_path):
    os.makedirs(output_path)

  mg5_input_file = os.path.join(cudir,"run.mg5")
  with open(mg5_input_file, "w") as mg5_input:
    with open(os.path.join("/fsa/home/sjc_caiyz/samples/signal", "mg5"), "r") as mg5_build:
      mg5_input.write(mg5_build.read().format(r = arg, iseed1 = str(int(arg.iseed)+10), iseed2 = str(int(arg.iseed)+10), output_path = output_path, process = process))
  mg5_executable = subprocess.Popen([os.path.join(dir, "bin", "mg5_aMC"), mg5_input_file])
  mg5_executable.communicate()
  if mg5_executable.returncode:
    raise OSError("MG5 Error")

  #subprocess.call(['rm',os.path.join(output_path,'Events/run_01/tag_1_pythia8_events.hepmc')])
  #subprocess.call([os.path.join(output_path,'bin/madevent'), 'remove', 'all', 'pythia', '-f'])

  #shutil.copyfile(os.path.join(output_path,'Events','run_01','tag_1_delphes_events.root'),os.path.join(dir,"../diHiggs","{}.root".format(filename)))
  #print("Copy finished""")


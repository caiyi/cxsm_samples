module load anaconda/3-5.0.1
. /fs00/software/anaconda/3-5.0.1/etc/profile.d/conda.sh
export LD_LIBRARY_PATH=/lib64:$LD_LIBRARY_PATH
module load gcc/8.2.0
module load cmake/3.16.3
module load gnuplot/5.2.7
source /fsa/home/sjc_caiyz/root/bin/thisroot.sh
conda activate mine

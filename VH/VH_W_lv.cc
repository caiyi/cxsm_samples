#include "Pythia8/Pythia.h"
#include "Pythia8Plugins/HepMC2.h"
#include "Pythia8/HadronLevel.h"
#include <cstdio>
#include <fstream>
#include <sstream>
#include <cmath>
#include <string>
#include <vector>
#include <cstdlib>
using namespace Pythia8;
using namespace std;
using std::vector;
int main() {

  
  // Interface for conversion from Pythia8::Event to HepMC event.
  HepMC::Pythia8ToHepMC ToHepMC;

  // Specify file where HepMC events will be stored.
  HepMC::IO_GenEvent ascii_io("/fsa/home/sjc_caiyz/samples/VH/VH_W_lv.hepmc", std::ios::out);

  // Intialize Pythia.
  Pythia pythia;

  // Process selection.
  //pythia.readString("WeakSingleBoson:ffbar2gmZ = on");
  pythia.readString("HiggsSM:ffbar2HW = on");
  pythia.readString("PhaseSpace:minWidthBreitWigners = 0.001");
  pythia.readString("25:onMode = off");
  pythia.readString("25:onIfMatch = 5 -5"); 
  pythia.readString("24:onMode = off");
  pythia.readString("24:onIfAny = 11 12 13 14 15 16");
  pythia.readString("PhaseSpace:pTHatMin = 5.");
  pythia.readString("SigmaProcess:alphaSvalue = 0.12");
  pythia.readString("SpaceShower:alphaSvalue = 0.13");
  pythia.readString("HadronLevel:Hadronize = on");
  // CEPC initialization at 240 GeV.
  pythia.readString("Beams:idA =  2212");
  pythia.readString("Beams:idB =  2212");
  //double mZ = pythia.particleData.m0(23);
  pythia.settings.parm("Beams:eCM", 14000);

  pythia.readString("Random:setSeed = on");
  pythia.readString("Random:seed = 999");

  pythia.init();

  // Set the number of events in the generator file.
  int nEvtPerFile = 1000000;

  // The event loop.
  //Hist mass("m(e, pi0) [GeV]", 100, 0., 2.);
  //Hist mult("charged multiplicity", 100, -0.5, 799.5);
  for (int iEvent = 0; iEvent < nEvtPerFile; ) {
    // Generate the event.
    if (!pythia.next()) continue;
    // Analyze the event.
    //Event &event = pythia.event;
	// Construct new empty HepMC event and fill it.
	// Units will be as chosen for HepMC build; but can be changed
	// by arguments, e.g. GenEvt( HepMC::Units::GEV, HepMC::Units::MM)
	
	// Write the HepMC event to file. Done with it.
	HepMC::GenEvent* hepmcevt = new HepMC::GenEvent();
	ToHepMC.fill_next_event( pythia, hepmcevt );
	ascii_io << hepmcevt;


	iEvent++;
	delete hepmcevt;
  }

  // Print the statistics and histogram.
  pythia.stat();
  return 0;
}
